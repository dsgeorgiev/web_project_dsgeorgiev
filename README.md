# WEB_Project_DSGeorgiev

We implemented the API in our shop. Follow the steps to make it work : 
---Open terminal/powershell
---Navigate to our project folder
---Start the server using "python server.py" command
---Open browser
---Open http://localhost:8080/
---You will end up in the index.html

Using the menu you can navigate to the API Documentation


We included the WCAG report as separated item in the menu of our webshop. 
Please note  that Appendix A is only accessible from the WCAG 2.0 GUIDELINES REPORT menu item.