//used when sorting the table
function sortTable(colmnNumber) {
    //get table, its rows and extra variable to keep track if we need to order
    var table, rows, switching, i, x, y, shouldSwitch;
    table = document.getElementById("products");
    //set reorder to true
    switching = true;
    //loop while reorder is true
    while (switching) {
        //consider that reorder is not needed anymore
        switching = false;
        rows = table.rows;
        //go trough all table rows  excludingf the first and last one, hence first is header row and last on is uised to add prodcuts
        for (i = 1; i < (rows.length - 2); i++) {
            //consider that reorder is not needed anymore
            shouldSwitch = false;
            //get all elements of type td(currnt row) 
            x = rows[i].getElementsByTagName("TD")[colmnNumber];
            //get all elements of type td(next row)
            y = rows[i + 1].getElementsByTagName("TD")[colmnNumber];
            //compare the data
            if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                shouldSwitch = true;
                break;
            }
        }
        //if reorder is stil true change the order of the rows
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}

//delete all products from the database
function resetTable(){
    //get table 
    var table = document.getElementById("products");
    var tableRows = table.rows.length;
    
    //For some wierd reason the database cannot be reset
    var xhttp = new XMLHttpRequest();
    xhttp.open("DELETE", "http://localhost:8080/products", true);
    xhttp.send();  

    //go trough all of the rows and delte them dinamically 
    for(var i = 1; i<tableRows-1; i++){
        table.deleteRow(1);
    }
    //reload the data
    readData();                        
}

//used when user is adding a product
function insertRow() {

    //if the data is not in valid format return 0
    if(!validate()){
        return 0;
    }
   
    //get table ana add new row
    var table = document.getElementById("products"); 

    //get the data form all of teh input elements
    var productNameValue = document.getElementById("product").value;
    var productOriginValue = document.getElementById("origin").value;
    var productBestBeforeValue = document.getElementById("best_before_date").value;
    var productAmountValue = document.getElementById("amount").value;
    var productImageValue = document.getElementById("image").value;
    
    //new post request 
    const xhr = new XMLHttpRequest();
    
    xhr.onload = function(){

        var tableRows = table.rows.length;
        for(var i = 1; i<tableRows-1; i++){
            table.deleteRow(1);
        }
        //reload the data
        readData();
    }
    xhr.open("POST", "http://localhost:8080/products");
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify({ "product": ""+productNameValue+"", "origin": ""+productOriginValue+"","best_before": ""+productBestBeforeValue+"","amount": ""+productAmountValue+"","image_url": ""+productImageValue+""}));
}

//Popup "window" asking if the user is sure that they want to reset the table
function resetQuestion(){
    //get the hidden div
    var warningWindow = document.getElementById("reset-confirm-window-id");

    //show the hiden div
    warningWindow.style.display = "block";

    //reset the table if the confirm botton has been clicked and hide the div
    var confirmBtn = document.getElementById("confirm-button-id");
    confirmBtn.onclick = function(){
        resetTable();
        warningWindow.style.display = "none";
    }

    //jiust hide the div if cancel was clicked 
    var cancelBtn = document.getElementById("cancel-button-id");
    cancelBtn.onclick = function() {
        warningWindow.style.display = "none";
    }

    //hide the div if the cloise button was clicked
    var span = document.getElementsByClassName("close")[0];
    span.onclick = function() {
        warningWindow.style.display = "none";
    }
}

//This method is used to read the data from teh database when the paga is opend
function readData(){

    console.log("Welcome to the online store of CS02-025!!!\nFeel free to play aroud and add some products.");
    var table = document.getElementById("products");
    
    var xmlhttp = new XMLHttpRequest(); //create new HttpRequest instance
    xmlhttp.open("GET", "http://localhost:8080/products"); //open the instance
    xmlhttp.onreadystatechange = function() {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
            var resp = JSON.parse(xmlhttp.responseText.toString());
            //this resp object will hold the response form the request which is array from products
            for(var i=0; i<resp.products.length; i++){
                
                //insert all products from the database in rolls
                var row = table.insertRow(i+1);
                var productID = row.insertCell(0);
                var productName = row.insertCell(1);
                var productOrigin = row.insertCell(2);
                var prodcutBestBefore = row.insertCell(3);
                var productAmount = row.insertCell(4);
                var productimage = row.insertCell(5);
                var productEditButton = row.insertCell(6);
                var productDeleteButton = row.insertCell(7);
                
                productimage.colSpan = "2";
                productimage.style.textAlign = "center";

                productID.innerHTML = resp.products[i].id;
                productName.innerHTML = resp.products[i].product;
                productOrigin.innerHTML = resp.products[i].origin;
                prodcutBestBefore.innerHTML = resp.products[i].best_before;
                productAmount.innerHTML = resp.products[i].amount + "kg";
                productimage.innerHTML = "<img src=\""+resp.products[i].image_url+"\" alt=\"This is "+resp.products[i].product+" image\" width=150/>";
                productEditButton.innerHTML = "<button class=\"edit-button\" id=\"editButton"+(i+1)+"\" onclick=\"update("+(i+1)+")\">Edit</button>";
                productDeleteButton.innerHTML = "<button class=\"delete-button\" id=\"deleteButton"+(i+1)+"\" onclick=\"deleteQuestion("+(i+1)+")\">Delete</button>";
            }
            
        }
    };
    xmlhttp.send()

    //reset teh input values
    document.getElementById("product").value = "";
    document.getElementById("origin").value = "";;
    document.getElementById("best_before_date").value = "";
    document.getElementById("amount").value = "";
    document.getElementById("image").value = "";
}

function validate(){
    var productNameValue = document.getElementById("product").value;
    var productOriginValue = document.getElementById("origin").value;
    var productBestBeforeValue = document.getElementById("best_before_date").value;
    var productAmountValue = document.getElementById("amount").value;
    var productImageValue = document.getElementById("image").value;
    var check = false;
    if(productNameValue=="" || productOriginValue=="" || productBestBeforeValue=="" || productAmountValue=="" || productImageValue==""){
        //alert if empty element
        alert("Please make sure that none of the fields are empty!");
        check = false;
    }else if(isNaN(productAmountValue)){
        //alert if amount is not an int numebr
        alert("Please make sure that the Amount of the product is an actual number!");
        check = false;
    }else if(!isUrl(productImageValue)){
        //alert if the provided url is not valid
        alert("Please make sure that the Image link you provided is an actual valid URL!\nExample: https://www.example.com/image.png/");
        check = false;
    }else{check = true;}
    
    return check;
}

//Chedck if url is valid using regular expressions 
function isUrl(url) {
    var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
    return regexp.test(url);
 }

 //Update product

 function update(tableObject){
    var table = document.getElementById("products");
    var productID = table.rows[tableObject].cells.item(0).innerHTML;


    var productNameValue = "";
    var productOriginValue= "";
    var productBestBeforeValue = "";
    var productAmountValue = "";
    var productImageValue = "";

    var productNameCell = table.rows[tableObject].cells.item(1);
    var productOriginCell = table.rows[tableObject].cells.item(2);
    var productBestBeforeCell = table.rows[tableObject].cells.item(3);
    var productAmountCell = table.rows[tableObject].cells.item(4);
    var productImageCell = table.rows[tableObject].cells.item(5);
    var editButtonConfirm = table.rows[tableObject].cells.item(6);
    
    //get the existing product form the database depending on the id
    var xmlhttp = new XMLHttpRequest(); //create new HttpRequest instance
    xmlhttp.open("GET", "http://localhost:8080/products/"+productID); //open the instance
    xmlhttp.onreadystatechange = function() {
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
            var resp = JSON.parse(xmlhttp.responseText.toString());
            //this resp object will hold the response form the request which is array from products
            for(var i=0; i<resp.products.length; i++){

                productNameValue = resp.products[i].product;
                productOriginValue = resp.products[i].origin;
                productBestBeforeValue = resp.products[i].best_before;
                productAmountValue = resp.products[i].amount;
                productImageValue = resp.products[i].image_url;
                 
                //Get all of the row elements and turn them in input types so the user can edit them
                productNameCell.innerHTML = "<input type=\"text\" id=\"product_update\" name=\"product_update\" value=\""+productNameValue+"\"/>";
                productOriginCell.innerHTML = "<input type=\"text\" id=\"origin_update\" name=\"origin_update\" value=\""+productOriginValue+"\"/>";
                productBestBeforeCell.innerHTML = "<input type=\"text\" id=\"best_before_date_update\" name=\"best_before_date_update\" value=\""+productBestBeforeValue+"\"/>";
                productAmountCell.innerHTML = "<input type=\"t  ext\" id=\"amount_update\" name=\"amount_update\" value=\""+productAmountValue+"\"/>";
                productImageCell.innerHTML = "<input type=\"text\" id=\"image_update\" name=\"image_update\" value=\""+productImageValue+"\"/>";
                
                //change the button to play role of an confimation for the update and call the actual produc update function
                document.getElementById("editButton"+tableObject).innerHTML = "Confirm";
                editButtonConfirm.onclick = function(){
                    confirmUpdate(productID,document.getElementById("product_update").value,document.getElementById("origin_update").value,document.getElementById("best_before_date_update").value,document.getElementById("amount_update").value,document.getElementById("image_update").value);
                 };
            }
        }
    };
    xmlhttp.send()
    
 }
//Actual prduct update
 function confirmUpdate(productID,productNameValueUpdated,productOriginValueUpdated,productBestBeforeValueUpdated,productAmountValueUpdated,productImageValueUpdated){
    console.log(productID+ " "+productNameValueUpdated+ " "+productOriginValueUpdated+ " "+productBestBeforeValueUpdated+ " "+productAmountValueUpdated+ " "+productImageValueUpdated);
    var table = document.getElementById("products");
    const xhr = new XMLHttpRequest();
    xhr.onload = function(){
        var tableRows = table.rows.length;
        for(var i = 1; i<tableRows-1; i++){
            table.deleteRow(1);
        }
        //reload data
        readData();
    }
    xhr.open("PUT", "http://localhost:8080/products/"+productID);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify({ "product": ""+productNameValueUpdated+"", "origin": ""+productOriginValueUpdated+"","best_before": ""+productBestBeforeValueUpdated+"","amount": ""+productAmountValueUpdated+"","image_url": ""+productImageValueUpdated+""})); 
 }

 //Ask confirmation form user to delete product
 function deleteQuestion(rowID){

    var table = document.getElementById("products");
    var productID = table.rows[rowID].cells.item(0).innerHTML;

      //get the hidden div
      var warningWindow = document.getElementById("delete-confirm-window-id");

      //show the hiden div
      warningWindow.style.display = "block";
  
      //reset the table if the confirm botton has been clicked and hide the div
      var confirmBtn = document.getElementById("confirm-delete-button-id");

      confirmBtn.onclick = function(){
        warningWindow.style.display = "none";
        
        deleteProduct(productID);       
      }
  
      //jiust hide the div if cancel was clicked 
      var cancelBtn = document.getElementById("cancel-delete-button-id");
      cancelBtn.onclick = function() {
          warningWindow.style.display = "none";
      }
  
      //hide the div if the cloise button was clicked
      var span = document.getElementsByClassName("close")[0];
      span.onclick = function() {
          warningWindow.style.display = "none";
      }
 }

 //Delete single porduct depending on selected ID from the table
 function deleteProduct(productID){
    var table = document.getElementById("products");
    //For some wierd reason the database cannot be reset
    var xhttp = new XMLHttpRequest();
    xhttp.open("DELETE", "http://localhost:8080/products/"+productID, true);
    xhttp.send();  

    var tableRows = table.rows.length;
    for(var i = 1; i<tableRows-1; i++){
        table.deleteRow(1);
    }
    //Reload the table
    readData();                        
 }

