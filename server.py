import sqlite3
from bottle import response, static_file
from json import dumps
from bottle import run, get, route, request, post, delete, put, error, HTTPResponse

conn = sqlite3.connect('products.db')

#  importing all static files needed to run on the server 
@route('/script.js')
def js_file_static():
    return static_file('script.js', root='./')

@route('/coloring_script.js')
def js_coloring_file_static():
    return static_file('coloring_script.js', root='./')
    
@route('/report.html')
def report_file_static():
    return static_file('report.html', root='./')

@route('/appendixa.html')
def appendix_file_static():
    return static_file('appendixa.html', root='./')


@route('/screen.css')
def screen_css_file_static():
    return static_file('screen.css', root='./')

@route('/request.css')
def request_css_file_static():
    return static_file('request.css', root='./')
    

@route('/api_documentation.html')
def api_doc_file_static():
    return static_file('api_documentation.html', root='./')

@route('/')
def index_file_static():
    return static_file('index.html', root='./')

    #################################################

@get('/products')
def getAllProducts():

    # conn.execute("DELETE FROM product")
    # conn.execute("INSERT INTO product (product,origin,best_before,amount,image_url) VALUES ('Apples','The Netherlands','01/01/2020',100,'https://upload.wikimedia.org/wikipedia/commons/thumb/e/ee/Apples.jpg/512px-Apples.jpg')")
    # conn.execute("INSERT INTO product (product,origin,best_before,amount,image_url) VALUES ('Banana','India','01/02/2020',120,'https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Bananas.jpg/640px-Bananas.jpg')")
    # conn.commit()

    c = conn.cursor()
    c.execute("SELECT * FROM product")
    result = c.fetchall()

    allProducts = []

    for row in result:
        allProducts.append({'id': row[0],'product': row[1],'origin': row[2],'best_before': row[3],'amount': row[4],'image_url': row[5]})

    response.content_type = 'application/json'
    return dumps({'products':allProducts})

@get('/products/<product_id>')
def getSelectedProduct(product_id):
    c = conn.cursor()
    c.execute("SELECT * FROM product WHERE id LIKE " + product_id)
    result = c.fetchall()
    if not result:
        raise HTTPResponse(
                status=404,
                body={ "error": "404", "description": "id not found", "info": "The requested id does not exist in the database"}
        )
    else:
        c = conn.cursor()
        c.execute("SELECT * FROM product WHERE id LIKE " + product_id)
        result = c.fetchall()

        allProducts = []

        for row in result:
            allProducts.append({'id': row[0],'product': row[1],'origin': row[2],'best_before': row[3],'amount': row[4],'image_url': row[5]})

        response.content_type = 'application/json'
        return dumps({'products':allProducts})

@post('/products')
def addnewProduct():
    response.content_type = 'application/json'
    newProduct = request.json
    product = newProduct['product']
    origin = newProduct['origin']
    best_before = newProduct['best_before']
    amount = newProduct['amount']
    image_url = newProduct['image_url']
    
    conn.execute("INSERT INTO product (product,origin,best_before,amount,image_url) VALUES ('"+product+"','"+origin+"','"+best_before+"',"+str(amount)+",'"+image_url+"')")
    conn.commit()

    c = conn.cursor()
    c.execute("SELECT * FROM product")
    result = c.fetchall()

    allProducts = []

    for row in result:
        allProducts.append({'id': row[0],'product': row[1],'origin': row[2],'best_before': row[3],'amount': row[4],'image_url': row[5]})

    response.content_type = 'application/json'
    return dumps({'product':allProducts})

@delete('/products/<product_id>')
def deleteSelected(product_id):
    c = conn.cursor()
    c.execute("SELECT * FROM product WHERE id LIKE " + product_id)
    result = c.fetchall()
    if not result:
        raise HTTPResponse(
                status=404,
                body={ "error": "404", "description": "id not found", "info": "The requested id does not exist in the database"}
        )
    else:
        conn.execute("DELETE FROM product WHERE id LIKE " + product_id)
        conn.commit()

        c = conn.cursor()
        c.execute("SELECT * FROM product")
        result = c.fetchall()

        allProducts = []

        for row in result:
            allProducts.append({'id': row[0],'product': row[1],'origin': row[2],'best_before': row[3],'amount': row[4],'image_url': row[5]})

        response.content_type = 'application/json'
        return dumps({'products':allProducts})

@delete('/products')
def deleteAll():
    conn.execute("DELETE FROM product")
    conn.commit()

    c = conn.cursor()
    c.execute("SELECT * FROM product")
    result = c.fetchall()

    allProducts = []

    for row in result:
        allProducts.append({'id': row[0],'product': row[1],'origin': row[2],'best_before': row[3],'amount': row[4],'image_url': row[5]})

    response.content_type = 'application/json'
    return dumps({'products':allProducts})

@put('/products/<product_id>')
def updateSelectedProdcut(product_id):
    response.content_type = 'application/json'
    newProduct = request.json
    product = newProduct['product']
    origin = newProduct['origin']
    best_before = newProduct['best_before']
    amount = newProduct['amount']
    image_url = newProduct['image_url']
    
    conn.execute("UPDATE product SET product = '"+product+"', origin = '"+origin+"', best_before = '"+best_before+"', amount = "+str(amount)+", image_url = '"+image_url+"' WHERE id LIKE "+product_id)
    conn.commit()

    c = conn.cursor()
    c.execute("SELECT * FROM product")
    result = c.fetchall()

    allProducts = []

    for row in result:
        allProducts.append({'id': row[0],'product': row[1],'origin': row[2],'best_before': row[3],'amount': row[4],'image_url': row[5]})

    response.content_type = 'application/json'
    return dumps({'product':allProducts})

@error(404)
def error404(error):
    error = [{ "error": "404", "description": "Not Found", "info": "The requested resource could not be found but may be available in the future. Subsequent requests by the client are permissible"}]
    response.content_type = 'application/json'
    return dumps(error)

@error(500)  
def error500(error):
    error = [{ "error": "500", "description": "Internal Server Error", "info": "A generic error message, given when an unexpected condition was encountered and no more specific message is suitable."}]
    response.content_type = 'application/json'
    return dumps(error)

@error(421)  
def error420(error):
    error = [{ "error": "421", "description": "Misdirected Request", "info": "The request was directed at a server that is not able to produce a response (for example because of connection reuse)."}]
    response.content_type = 'application/json'
    return dumps(error)

@error(405)  
def error405(error):
    error = [{ "error": "405", "description": "Method Not Allowed", "info": "A request method is not supported for the requested resource; for example, a GET request on a form that requires data to be presented via POST, or a PUT request on a read-only resource."}]
    response.content_type = 'application/json'
    return dumps(error)

if __name__ == '__main__':
    run(debug=True, reloader=True)